// Do NOT use .map, to complete this function.
// How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
// Produces a new array of values by mapping each value in list through a transformation function (iteratee).
// Return the new array.
const { array } = require("./Arrays.js")
const map = (elements, cb) => {
    if(!elements || !cb || !Array.isArray(elements) || typeof cb !== "function" ) return;
    const sendBack = [];
    for (let i = 0; i < elements.length; i++) {
        sendBack.push(cb(elements[i], i, elements));
    }
    return sendBack;
}

  module.exports = map;