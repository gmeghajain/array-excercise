// Do NOT use .includes, to complete this function.
// Look through each value in `elements` and pass each element to `cb`.
// If `cb` returns `true` then return that element.
// Return `undefined` if no elements pass the truth test.
const { array } = require("../Arrays.js");
const find = require("../find.js");

const cb = (ele, i) => ele > 7;
const ans = find(array, cb);

console.log(ans);
