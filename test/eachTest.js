// Do NOT use forEach to complete this function.
// Iterates over a list of elements, yielding each in turn to the `cb` function.
// This only needs to work with arrays.
// You should also pass the index into `cb` as the second argument
const { array } = require("../Arrays.js");
const each = require("../each.js");

const cb = (ele, i) => console.log( i,ele );
const result = each(array, cb);
