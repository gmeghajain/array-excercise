// Do NOT use .map, to complete this function.
// How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
// Produces a new array of values by mapping each value in list through a transformation function (iteratee).
// Return the new array.
const { array } = require("../Arrays.js");
const map = require("../map.js");

const cb = (ele, i, arr) => ele * ele;
const ans = map(array, cb);

console.log(ans);
