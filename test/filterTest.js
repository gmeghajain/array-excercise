// Do NOT use .filter, to complete this function.
// Similar to `find` but you will return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test
const { array } = require("../Arrays.js");
const filter = require("../filter.js");

const cb = (ele) => ele % 2 != 0; //new array will have only odd elements
const ans = filter(array, cb);

console.log(ans);
