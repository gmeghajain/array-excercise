// Do NOT use .reduce to complete this function.
// How reduce works: A reduce function combines all elements into a single value going from left to right.
// Elements will be passed one by one into `cb` along with the `startingValue`.
// `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
// `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
function reduce(elements, cb, startingValue) {
    if (!cb || !elements || !Array.isArray(elements) || typeof cb !== "function") return;
    let i = 0;
    if (!startingValue) {
        startingValue = elements[i];
        i++;
    }
    while(i<elements.length){
        startingValue = cb(startingValue, elements[i], i, elements);
        i++;
    }
    return startingValue;
}
module.exports= reduce;