// Do NOT use .filter, to complete this function.
// Similar to `find` but you will return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test
const filter=(elements, cb)=>{
    if (!elements || !cb || !Array.isArray(elements) || typeof cb !== "function") return;

    const sendBack = [];
    for (let i = 0; i < elements.length; i++) {
      if (cb(elements[i], i, elements)) {
          sendBack.push(elements[i]);
        }
    } 
    return sendBack;
}
module.exports = filter;