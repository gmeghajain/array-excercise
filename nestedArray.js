//const nestedArray = [1, [2], [[3]], [[[4]]]];
// use this to test 'flatten'

const nestedArray = [1, [2], [[3]], [[[4]]]]; 

module.exports.nestedArray = nestedArray;
