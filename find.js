// Do NOT use .includes, to complete this function.
// Look through each value in `elements` and pass each element to `cb`.
// If `cb` returns `true` then return that element.
// Return `undefined` if no elements pass the truth test.
const { array } = require("./Arrays.js")
const find=(elements, cb)=>{
    if(!elements || !cb || !Array.isArray(elements) || typeof cb !== "function" ) return;
    let i=0;
    while( i < elements.length ){
        if (cb(elements[i], i, elements)) {
          return elements[i];
        }
        i++;
    }   
}
module.exports = find